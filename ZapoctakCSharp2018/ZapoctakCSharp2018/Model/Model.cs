﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using Xamarin;
using Xamarin.Forms;


namespace ZapoctakCSharp2018
{
    /* ---- User classes ----     
     *  - for saving users settings, progress, personal data, preferences, etc.    
     * --------------------------------- */
    
    public class User
    {
        public string Name { get; set; }
        public UserProgress Progress { get; set; }
        public UserSetting Settings { get; set; }
        public Favourites Favourites { get; set; }
    }
        
    public class UserSetting
    {
        public bool ProgressSwitchedOn { get; private set; } = true;
    }

    public class Favourites
    {
        public CustomObservableCollection<Word> FavouriteWords { get; set; }
    }


    /* ---- Progress classes ----
     * Progress system:
     * The progress structure is very similar to the structure of the book:    
     * - UserProgress
     * -- LessonProgresses
     * --- ItemProgresses        
     * --------------------------- */

    [Serializable]
    [XmlRoot("UserProgress")]
    public class UserProgress
    {
        public List<LessonProgress> LessonsProgress { get; set; }                
    }

    [Serializable]
    public class LessonProgress
    {
        public string Name { get; set; }
        public ObservableCollection<ItemProgress> ItemsProgress { get; set; }        
    }

    /// <summary>
    /// Every Item (doesn't matter which class) has maximum progress (default 1) and actual progres.    
    /// </summary>
    [Serializable]
    public class ItemProgress
    {
        public string Name { get; set; }

        [XmlElement("Progress")]
        public int ActualProgress { get; set; } = -1;
        [XmlElement("Maximum")]
        public int MaximumProgress { get; set; } = 1;
    }


    /* ---- Classes containing the whole book ----
     *   - the symbolic structure of the book: 
     *   
     *   -Book
     *     - Lesson
     *       - LessonItem
     *         - LessonContent
     *   
     *   - the data loading is provided by XMLDataIO class and these classes don't "care" about reading and loading       
     * --------------------------------- */

    [XmlRoot("Book")]
    public class Book
    {        
        [XmlElement(typeof(Lesson),ElementName = "Lesson")]
        public List<Lesson> Lessons { get; set; }
    }

    [Serializable]
    public class Lesson
    {
        [XmlElement("Name")]
        public string LessonName { get; set; }

        [XmlElement("Path")]
        public string Path { get; set; }
                
        [XmlArray("LessonItems")]        
        [XmlArrayItem(typeof(GrammarItem), ElementName = "GrammarItem")]
        [XmlArrayItem(typeof(VocabularyItem), ElementName = "VocabularyItem")]
        [XmlArrayItem(typeof(PracticeWithCheckItem), ElementName = "PracticeCheck")]
        [XmlArrayItem(typeof(PracticeWithNoCheckItem), ElementName = "PracticeNoCheck")]
        public List<LessonItem> LessonItems { get; set; }
        
    }


    /* ---- Lesson Item classes ----
     * LessonItem class is abstract and contains Name property, Path to the xml file,
     * and list of LessonContent. (LessonContent list isn't used in every inherited type of LessonItem).
     * 
     * Grammar Item -> only for explaining grammar, can contain tables, simple text, conversations
     * Vocabulary Item -> holds list of words
     * Practice With Check -> holds list of Word objects (in Word object can be saved actual word but sentence too...)
     * Practice Without Check -> holds contents
     * --------------------------------- */

    [Serializable]
    public abstract class LessonItem
    {
        [XmlElement("Name")]
        public string Name { get; set; }
        
        // Path to file with LessonItem
        [XmlElement("Path")]
        public string Path { get; set; }
        
        [XmlArray("Contents")]
        [XmlArrayItem(typeof(TextContent), ElementName = "TextContent")]
        [XmlArrayItem(typeof(ConversationContent), ElementName = "ConversationContent")]
        [XmlArrayItem(typeof(TableContent), ElementName = "TableContent")]
        public List<LessonContent> LessonContents { get; set; }
    }    
    
    [XmlRoot("PracticeNoCheck")]
    public class PracticeWithNoCheckItem : LessonItem
    {
        
    }
    
    [XmlRoot("PracticeCheck")]
    public class PracticeWithCheckItem : LessonItem
    {
        [XmlElement("WhatToDo")]
        public string WhatToDo { get; set; }

        [XmlArray("ToTranslate")]
        public List<Word> ListToTranslate { get; set; }
    }

    public class VocabularyItem : LessonItem
    {
        [XmlArray("Vocabulary")]
        [XmlArrayItem("Word")]
        public List<Word> Vocabulary { get; set; }
    }
    
    public class GrammarItem : LessonItem
    {

    }



    /* ---- Word and Sentence classes ----
     * good for - comfortable work with translations 
     *          - comfortable work with serialization
     * --------------------------------- */

    public class Word
    {
        [XmlElement("Orig")]
        public string OriginalWord { get; set; }
        
        [XmlArray("Ts")]
        [XmlArrayItem("T")]
        public List<string> Translations { get; set; }
        
        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Word w = (Word)obj;
                return (OriginalWord == w.OriginalWord);
            }
        }        
    }

    
    public class Sentence
    {
        [XmlElement("A")]
        public string Author { get; set; }
        [XmlElement("S")]
        public string SentenceString { get; set; }
    }


    /* ---- Content classes ----     
     * When adding another classes:
     *   - add generating View of new class in ViewsGenerator class
     *   - add ViewModel code on pages classes
     * --------------------------------- */

    [XmlInclude(typeof(TextContent))]
    [XmlInclude(typeof(ConversationContent))]
    [XmlInclude(typeof(TableContent))]
    public abstract class LessonContent
    {
        [XmlElement("Name")]
        public string Name { get; set; }
    }
        
    public class ImageContent : LessonContent
    {
        [XmlElement("Path")]
        public string PathToImage { get; set; }
    }

    public class TextContent : LessonContent
    {
        [XmlElement("Text")]
        public string Text { get; set; }
    }

    public class ConversationContent : LessonContent
    {
        // first string in the tuple is the name, second actual sentence
        [XmlArray("Sentences")]
        [XmlArrayItem("Sentence")]
        public List<Sentence> Sentences = new List<Sentence>();

        [XmlElement("ShowNames")]
        public bool ShowDialoqueNames { get; set; } = true;
    }

    public class TableContent : LessonContent
    {
        /// <summary>
        /// example of possible table:
        ///  [Name] Slovesa
        ///  [TableHeader]       { Avere }
        ///  [Table]      { { avro, avrai ... } }
        /// </summary>
        
        public int NumberOfColumns { get; set; }

        [XmlElement("NameOfTable")]
        public string NameOfTable { get; set; }

        [XmlElement("Desc")]
        public string Description { get; set; }

        // Number of headers must be equal to NumberOfColumns
        [XmlArray("Headers")]
        [XmlArrayItem("Header")]
        public List<string> TableHeader { get; set; }

        // Every column can have different number of items
        [XmlArray("Table")]
        [XmlArrayItem("Column")]
        public List<List<string>> Table { get; set; }        
    }


    public class AnswersChecker
    {
        private static string[] ItalianArticles = 
            new string[] { "il", "la", "l'", "lo", "i", "gli", "le"};                

        public enum DistanceMethod
        {
            Levenstein
        }

        // removes article if the word begins with any, return the original string if not
        public static string RemoveArticle(string word)
        {
            foreach (var article in ItalianArticles)
            {
                if (word.StartsWith(article + " "))
                {
                    return word.Remove(0, article.Length + 1);
                }
                if (article == "l'" && word.StartsWith(article))
                {
                    return word.Remove(0, article.Length);
                }
            }
            return word;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="word">Translated word</param>
        /// <param name="answer">User's answer</param>
        /// <param name="checkArticle">Should the italian article be checked</param>
        /// <param name="distanceMethod"></param>
        /// <returns>string - the closest translation to user's answer, int - the distance</returns>
        public static ValueTuple<string,int> VocabularyChecker(Word word, string answer, bool checkArticle = true,
            DistanceMethod distanceMethod = DistanceMethod.Levenstein)
        {
            answer = answer.ToLower();
            foreach (var correct in word.Translations)
            {
                if (correct.ToLower() == answer)
                {
                    return (correct,0);
                }
            }

            if (!checkArticle)
            {
                string answerWithoutArticle = RemoveArticle(answer);
                foreach (var correct in word.Translations)
                {
                    if (RemoveArticle(correct) == answerWithoutArticle)
                    {
                        return (correct,0);
                    }
                }
            }

            string minString = "";
            int min = int.MaxValue;
            foreach (var correct in word.Translations)
            {
                int leven = LevensteinDistance(correct, answer);
                if (leven < min)
                {
                    min = leven;
                    minString = correct;
                }
            }

            return (minString,min);            
        }

        /// <summary>
        /// Returns the Levenstein Distance between two words
        /// </summary>
        /// <param name="s"></param>
        /// <param name="ss"></param>
        /// <returns></returns>
        public static int LevensteinDistance(string s, string ss)
        {
            int sLength = s.Length;
            int ssLength = ss.Length;

            if (sLength == 0) return ssLength;
            if (ssLength == 0) return sLength;

            int[][] dist = new int[sLength][];
            for (int i = 0; i < sLength; i++)
            {
                dist[i] = new int[ssLength];
                dist[i][0] = i;
            }
            for (int i = 0; i < ssLength; i++)
            {
                dist[0][i] = i;
            }

            for (int i = 1; i < sLength; i++)
            {
                for (int j = 1; j < ssLength; j++)
                {
                    int cost = 0;
                    if (s[i] != ss[j])
                    {
                        cost = 1;
                    }

                    dist[i][j] = Math.Min(dist[i - 1][j] + 1,
                        Math.Min(dist[i][j - 1] + 1, dist[i - 1][j - 1] + cost));
                }
            }
            return dist[sLength - 1][ssLength - 1];
        }        
    }
}
