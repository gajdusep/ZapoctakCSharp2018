﻿using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ZapoctakCSharp2018
{
    class PresetLabel : Label
    {       
        public PresetLabel()
        {
            HorizontalTextAlignment = TextAlignment.Start;
            HorizontalOptions = LayoutOptions.Center;
            TextColor = Color.Black;            
            // FontFamily = Device.RuntimePlatform == Device.Android ? "Slabo27px-Regular.ttf#Slabo27px-Regular" : null;            
        }       
    }

    class GreenFrame : Frame
    {
        public GreenFrame()
        {
            CornerRadius = 10;
            BackgroundColor = Color.LightGreen;
            Padding = 5;
            Margin = new Thickness(0, 5, 0, 0);
        }
    }

    class RoundFrame : Frame
    {
        public RoundFrame()
        {
            CornerRadius = 5;
            HasShadow = true;
            Margin = new Thickness(0, 0, 0, 0);
            Padding = new Thickness(5);
        }
    }

    public class ChangeSizeEditor : Editor
    {
        public ChangeSizeEditor()
        {
            this.TextChanged += (sender, e) =>
            {
                this.InvalidateMeasure();
            };
        }
    }

    public class BackgroundFrame : Frame
    {
        public BackgroundFrame()
        {
            BackgroundColor = Color.DarkGreen;
            HorizontalOptions = LayoutOptions.Fill;
            VerticalOptions = LayoutOptions.Center;
            Padding = new Thickness(2);
            Margin = new Thickness(5,5,5,5);
        }
    }    
}