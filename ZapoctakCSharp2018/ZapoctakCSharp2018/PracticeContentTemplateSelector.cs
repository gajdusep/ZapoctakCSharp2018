﻿using Xamarin.Forms;
using System;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018
{
    public class PracticeContentTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TableContentTemplate { get; set; }
        public DataTemplate ConversationContentTemplate { get; set; }
        public DataTemplate SimpleTextContentTemplate { get; set; }

        public PracticeContentTemplateSelector()
        { 
        }

        protected override DataTemplate OnSelectTemplate(object tuple, BindableObject container)
        {
            var tup = (ContentAnswerTuple)tuple;
            var item = tup.LessonContent;

            if (item is TableContent)
            {
                return TableContentTemplate;
            }
            if (item is ConversationContent)
            {
                return ConversationContentTemplate;
            }
            if (item is TextContent)
            {
                return SimpleTextContentTemplate;
            }
            return TableContentTemplate;
        }
    }    
}
