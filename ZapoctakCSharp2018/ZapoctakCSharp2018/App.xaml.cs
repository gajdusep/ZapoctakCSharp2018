﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018
{   
    public partial class App : Application
    {
        public static MainPageViewModel MainPageViewModel { get; set;}
        
        public static User User { get; set; }

		public App ()
		{            
            InitializeComponent();

            User = new User
            {
                Name = "Some Name"                                     
            };            
            Properties.Add("User", User);
            
            Task<UserProgress> userProgressTask = Task.Run<UserProgress>(async () => await LoadUserProgress());
            User.Progress = userProgressTask.Result;
            Properties.Add("UserProgress", User.Progress);

            MainPage = new NavigationPage(new MainPage(MainPageViewModel));

            Task<Favourites> userFavsTask = Task.Run<Favourites>(async () => await LoadFavourites());
            User.Favourites = userFavsTask.Result;
            FavouritesPageViewModel FavouritesViewModel = new FavouritesPageViewModel(User.Favourites, MainPage.Navigation);
            Properties.Add("Favourites", FavouritesViewModel);

            ((NavigationPage)MainPage).BarBackgroundColor = Color.DarkGreen;
        }

        private static string UserFolderName = "UserFolder";
        private static string UserProgressFileName = "UserProgress.xml";
        private static string FavouritesFileName = "Favourites.xml";
                
        public async Task SaveFavourites(Favourites favourites)
        {
            await PCLStorageIO.SaveUserFile<Favourites>(favourites, UserFolderName, FavouritesFileName);
        }

        // Loads saved favourite words
        public async Task<Favourites> LoadFavourites()
        {
            Favourites fav = await PCLStorageIO.LoadUserFile<Favourites>(UserFolderName, FavouritesFileName);
            if (fav == null)
            {
                return new Favourites();
            }
            return fav;
        }

        public async Task SaveUserProgress(UserProgress userProgress)
        {
            await PCLStorageIO.SaveUserFile<UserProgress>(userProgress, UserFolderName, UserProgressFileName);
        }

        public async Task<UserProgress> LoadUserProgress()
        {            
            UserProgress up = await PCLStorageIO.LoadUserFile<UserProgress>(UserFolderName, UserProgressFileName).ConfigureAwait(false);
            if (up == null)
            {                
                return new UserProgress();
            }
            else
            {                
                return up;
            }            
        }

        protected override void OnStart ()
		{
            // Handle when your app starts
		}

		protected override void OnSleep ()
		{
            // OnSleep is invoked when the app is minimalized and before closing the app             
            Task.Run(async () => await SaveUserProgress(User.Progress));
            Task.Run(async () => await SaveFavourites(User.Favourites));            			
		}

		protected override void OnResume ()
		{
            Task.Run(async () => { User.Progress = await LoadUserProgress(); });
            Task.Run(async () => { User.Favourites = await LoadFavourites(); });
			// Handle when your app resumes
		}        
	}
}
