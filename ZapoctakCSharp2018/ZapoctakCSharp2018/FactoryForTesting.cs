﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZapoctakCSharp2018
{
    /// <summary>
    /// Debugging class for creating all kinds of contents, lessons etc...
    /// </summary>
    internal class FactoryForTesting
    {
        private static int ActLesson = 0;

        public static List<LessonContent> MakeContents()
        {
            var contents = new List<LessonContent>
            {
                new TableContent
                {
                    Table = new List<List<string>>
                    {
                        new List<string> { "prvniaervaervaervaer\n\naveavr\naerv\naerv",  "druhy",  "treti" , "ctvrty"},
                        new List<string> { "prvni1", "druhy1", "treti1", "ctvrty1\naerv\naerv\naer\vnaervaerv"},
                        new List<string> { "ctvrty2"},
                    }
                },
                new ConversationContent
                {
                    Name = "nove jmeno",
                    Sentences =
                    {
                        new Sentence { Author = "prvni", SentenceString="jsem autor jedna a rikam nejakou blbost"},
                        new Sentence { Author = "druhy", SentenceString= "jsem druhy autor a rikam jeste vetsi blbost"}
                    }
                },
                new TableContent
                {
                    Table = new List<List<string>>
                    {
                        new List<string> { "prvniaervaervaervaer\n\naveavr\naerv\naerv",  "druhy",  "treti" , "ctvrty"},
                        new List<string> { "prvni1", "druhy1", "treti1", "ctvrty1\naerv\naerv\naer\vnaervaerv"},
                        new List<string> { "ctvrty2", "druhy1", "treti1", "ctvrty" }
                    }
                }
            };
            return contents;
        }

        public static List<Word> GetWords(int k)
        {
            var vocab = new List<Word>
            {
                new Word { OriginalWord = "ahoj", Translations = new List<string> { "ciao" } },
                new Word { OriginalWord = "more", Translations = new List<string> { "il mare", "il sea" } },
                new Word { OriginalWord = "slunce", Translations = new List<string> { "il sole"} },
                new Word { OriginalWord = "zidle", Translations = new List<string> { "la sedia", "la seggiola" } }
            };

            List<Word> list = new List<Word>();
            for (int i = 0; i < k; i++)
            {
                Word v = GetWordChodit();
                v.OriginalWord += i.ToString();
                list.Add(v);                
            }            
            return list;
        }

        public static Word GetWordChodit()
        {
            return new Word
            {
                OriginalWord = "chodit",
                Translations = new List<string>()
                {
                    "andare", "camminare", "girare"
                }
            };
        }

        public static Lesson GetLesson()
        {
            Random rnd = new Random();

            if (ActLesson == 5)
            {
                ActLesson = 100;
            }
            var lesson = new Lesson
            {
                LessonName = ActLesson++.ToString()
            };                        

            return lesson;
        }

        public static List<Lesson> GetLessonList()
        {
            List<Lesson> list = new List<Lesson>();
            for (int i = 0; i < 15; i++)
            {
                list.Add(GetLesson());
            }
            return list;
        }

        public static TableContent GetTable1()
        {
            TableContent table = new TableContent();
            table.Name = "Slovesa";
            table.NumberOfColumns = 1;
            table.TableHeader = new List<string> { "avere" };
            table.Table = new List<List<string>>();
            table.Table.Add(new List<string> { "avro", "avrai", "avra", "avremo", "avrete", "avranno" });
            return table;
        }

        public static TableContent GetTable2()
        {
            TableContent table = new TableContent();
            table.Name = "Dosud probrana nepravidelna slovesa tvori budouci cas";
            table.NumberOfColumns = 2;
            table.NameOfTable = "pravidelne";
            table.TableHeader = new List<string> { "", "" };
            table.Table = new List<List<string>>();
            table.Table.Add(new List<string> { "dare", "dire", "fare", "riuscire", "Che cosa pensi di quel film?" });
            table.Table.Add(new List<string> { "daro", "diro", "faro", "riusciro", "Non ne penso un granche.\nNemyslim si o nem nic moc dalsiho blabla jeste trochu dost" });
            return table;
        }    

        public static TableContent GetTable3()
        {
            TableContent table = new TableContent();
            table.Name = "Rispondete al futuro";
            table.NumberOfColumns = 3;
            table.TableHeader = null;
            table.Table = new List<List<string>>();
            table.Table.Add(new List<string> { "quando" });
            table.Table.Add(new List<string> { "comprerai/comprera/comprerete la nuova moto",
                "lo domanderai/domandera/domandero/domanderamo",
                "lo domanderai/domandera/domandero/domanderamo" });
            table.Table.Add(new List<string> { "sabato", "domani", "stasera", "giovedi" });
            return table;
        }

        public static ConversationContent GetConversationContent1()
        {
            ConversationContent conversationContent = new ConversationContent();           
            conversationContent.Sentences = new List<Sentence>
            {
                new Sentence { Author = "Lui", SentenceString = "Ciao, come stai, non posso parlare blabablablab, new line \n\n qualcos'altro" },
                new Sentence { Author = "Lei", SentenceString = "Ehe, super, devo andare a casa, cosi ciao" },
                new Sentence { Author = "Lui", SentenceString ="questo era stata conversazione breva" }
            };
            return conversationContent;
        }

        public static ConversationContent GetConversationContent2()
        {
            ConversationContent conversationContent = new ConversationContent();
            conversationContent.ShowDialoqueNames = false;
            conversationContent.Sentences = new List<Sentence>
            {
                new Sentence { Author = "1", SentenceString = "Ciao Claudia, ti sei riposata al mare?" },
                new Sentence { Author ="2", SentenceString = "si, ho trascorso delle giornate bellissime, per me e stato un soggiorno ideale" },
                new Sentence { Author = "1", SentenceString = "bene" }
            };
            return conversationContent;
        }
    }
}
