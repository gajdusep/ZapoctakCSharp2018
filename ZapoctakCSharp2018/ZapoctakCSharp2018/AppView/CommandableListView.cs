﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;


namespace ZapoctakCSharp2018.AppView
{
    /// <summary>
    /// BY DAVID HAUCK
    /// https://github.com/davidhauck/ItemClickBinding
    /// Class for better work with ListViews -> allows us to bind commands
    /// Better for MVVM
    /// </summary>
    public class CommandableListView : ListView
    {
        public static BindableProperty ItemClickCommandProperty = 
            BindableProperty.Create(nameof(ItemClickCommand), typeof(ICommand), typeof(CommandableListView), null);

        public ICommand ItemClickCommand
        {
            get
            {
                return (ICommand)this.GetValue(ItemClickCommandProperty);
            }
            set
            {
                this.SetValue(ItemClickCommandProperty, value);
            }
        }

        public CommandableListView()
        {
            this.ItemTapped += OnItemTapped;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                ItemClickCommand?.Execute(e.Item);
                SelectedItem = null;
            }
        }
    }
}
