﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018.AppView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]    
    public partial class GreenFrame : Frame
	{
        public View ContentContent
        {
            get { return ContentView.Content; }
            set { ContentView.Content = value; }
        }

		public GreenFrame ()
		{
			InitializeComponent ();
		}
	}
}