﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin;
using System.IO;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ZapoctakCSharp2018.AppView;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using ZapoctakCSharp2018.ViewModel;
using PCLStorage;
using System.Diagnostics;
using System.Xml.Serialization;

namespace ZapoctakCSharp2018
{    
    public partial class MainPage : ContentPage
	{
        public MainPage(MainPageViewModel mainPageViewModel)
        {
            App.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
            InitializeComponent();            
            
            if (mainPageViewModel == null)
            {
                var up = (UserProgress)App.Current.Properties["UserProgress"];
                mainPageViewModel = new MainPageViewModel(Navigation, up);
            }
            BindingContext = mainPageViewModel;
		}                
    }    
}
