﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018.AppView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class VocabularyItemPageView : ContentPage
	{
		public VocabularyItemPageView (VocabularyItemPageViewModel viewModel)
		{
			InitializeComponent ();
            BindingContext = viewModel;
		}
	}
}