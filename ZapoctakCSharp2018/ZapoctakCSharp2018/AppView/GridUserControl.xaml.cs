﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018.AppView
{    
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GridUserControl : ContentView
	{
		public GridUserControl ()
		{
			InitializeComponent ();
		}

        public static readonly BindableProperty TableProperty =
            BindableProperty.Create(nameof(Table), typeof(TableContent), typeof(GridUserControl), null,
                BindingMode.OneWay, null, OnTableChanged);

        public TableContent Table
        {
            get { return (TableContent)GetValue(TableProperty); }
            set { SetValue(TableProperty, value); }
        }
        
        private static void RegularTable(Grid control, List<List<string>> table, int columns)
        {
            int rows = table[0].Count;
            control.RowSpacing = 2;
            control.ColumnSpacing = 2;

            // the number of rows is the same in every columns -> add rows definitions
            for (int i = 0; i < rows; i++)
            {
                control.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            }

            // go through the table and add labels
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    PresetLabel l = new PresetLabel() { Text = table[j][i], HorizontalTextAlignment = TextAlignment.Center };
                    Frame f = new Frame
                    {
                        CornerRadius = 0,
                        HasShadow = false,
                        Padding = new Thickness(1, 5, 1, 5),
                        Content = l
                    };
                    control.Children.Add(f, j, i);
                }
            }
        }

        private static void IrregularTable(Grid control, List<List<string>> table, int columns)
        {
            control.ColumnSpacing = 5;
            control.RowSpacing = 0;
            for (int i = 0; i < columns; i++)
            {
                StackLayout stackLayout = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.Center,
                    Spacing = 0
                };
                for (int j = 0; j < table[i].Count; j++)
                {
                    Label label = new PresetLabel { Text = table[i][j] };
                    Frame frame = new Frame { Content = label, Margin = 1, Padding = 2, HasShadow = false, CornerRadius = 0 };
                    stackLayout.Children.Add(frame);
                }
                control.Children.Add(stackLayout, i, 0);
            }
        }

        /// <summary>
        /// Function that defines a Grid (not in Xaml because we the grid (table) to be dynamic)
        /// </summary>
        private static void OnTableChanged(BindableObject bindableObject, object oldo, object newo)
        {
            var control = (ContentView)bindableObject;
            if (control == null) return;
            Grid grid = new Grid();
            StackLayout stackLayout = new StackLayout();
            control.Content = stackLayout;

            grid.BackgroundColor = Color.ForestGreen;

            if (newo is TableContent tableContent)
            {
                var table = tableContent.Table;
                int columns = table.Count;

                // if the table has Name
                if (tableContent.NameOfTable != null && tableContent.NameOfTable != "")
                {                                        
                    stackLayout.Children.Add(new Frame()
                    {
                        BackgroundColor = Color.LightGreen,
                        Padding = new Thickness(2),
                        Margin = new Thickness(2),
                        Content = new Label()
                        {
                            FontAttributes = FontAttributes.Bold,
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = tableContent.NameOfTable
                        }
                    });
                }

                // if the description is not empty
                if (tableContent.Description != null && tableContent.Description != "")
                {
                    stackLayout.Children.Add(new Label
                    {
                        Text = tableContent.Description
                    });
                }

                // check if the number of rows in each column is the same
                int someLength = table[0].Count;
                bool sameLength = true;
                for (int i = 1; i < columns; i++)
                {
                    if (someLength != table[i].Count)
                    {
                        sameLength = false;
                        break;
                    }
                }
                
                // the number of columns is the same for both regular and irregular table
                ColumnDefinitionCollection cdc = new ColumnDefinitionCollection();
                for (int i = 0; i < columns; i++)
                {
                    ColumnDefinition cd = new ColumnDefinition()
                    {
                        Width = new GridLength(1, GridUnitType.Star)
                    };
                    cdc.Add(cd);
                }

                // irregular table
                if (!sameLength)
                {
                    grid.ColumnDefinitions = cdc;
                    IrregularTable(grid, table, columns);
                }
                // regular table
                else
                {                                                
                    RegularTable(grid, table, columns);                        
                }
            }

            stackLayout.Children.Add(grid);
        }                    
    }
}