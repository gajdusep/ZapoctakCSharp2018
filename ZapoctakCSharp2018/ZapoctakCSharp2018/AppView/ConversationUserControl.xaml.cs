﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018.AppView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConversationUserControl : Grid
	{
		public ConversationUserControl ()
		{
			InitializeComponent ();            
		}        

        public static readonly BindableProperty ConversationContentItemProperty =
            BindableProperty.Create(nameof(ConversationContentItem), typeof(ConversationContent), typeof(ConversationUserControl), null,
                BindingMode.OneWay, null, OnConversationChange);
        
        public ConversationContent ConversationContentItem
        {
            get
            {
                return (ConversationContent)GetValue(ConversationContentItemProperty);
            }
            set
            {
                SetValue(ConversationContentItemProperty, value);
            }
        }

        /// <summary>
        /// Dynamic method for View of ConversationContent.
        /// This View is not defined in XAML, because of "list in list" structure
        ///     (listview in listview is unpropriate behaviour, therefore this method is defined)
        /// </summary>        
        private static void OnConversationChange(BindableObject bindableObject, object oldobject, object newobject)
        {            
            var contentview = (ConversationUserControl)bindableObject;
            if (contentview != null)
            {
                if (newobject is ConversationContent conversation)
                {                    
                    StackLayout sl = new StackLayout();
                    List<Color> colorList = ColorTools.GetColors(5);
                    int indexInColorList = 0;
                    Dictionary<string, Color> colorDict = new Dictionary<string, Color>();
                    
                    foreach (var sentence in conversation.Sentences)
                    {
                        Color cl;
                        if (!colorDict.TryGetValue(sentence.Author, out cl))
                        {
                            colorDict[sentence.Author] = colorList[indexInColorList];
                            indexInColorList++;
                            cl = colorDict[sentence.Author];
                        }

                        // Stringbuilder for faster working with strings
                        StringBuilder s = new StringBuilder("");
                        
                        if (conversation.ShowDialoqueNames)
                        {
                            s.Append(sentence.Author).Append(": ");
                        }
                        s.Append(sentence.SentenceString);

                        sl.Children.Add(new PresetLabel
                        {
                            Text = s.ToString(),
                            TextColor = cl
                        });
                    }

                    contentview.Children.Add(sl);                    
                }
            }            
        }
    }
}