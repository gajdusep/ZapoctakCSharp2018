﻿using ZapoctakCSharp2018.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018.AppView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PracticeCarouselPage : ContentPage
	{
		public PracticeCarouselPage (PracticeCarouselPageViewModel viewModel)
		{
			InitializeComponent ();
            BindingContext = viewModel;
		}
	}        
}