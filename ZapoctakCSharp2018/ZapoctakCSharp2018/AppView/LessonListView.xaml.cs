﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018.AppView
{    
    /// <summary>
    /// The Content View class, showing the list of lessons with details (lesson items)
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LessonListView : ContentPage
    {
        public LessonListView(LessonListPageViewModel lessonListViewModel)
        {
            InitializeComponent();
            BindingContext = lessonListViewModel;
        }        
    }                
}