﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ZapoctakCSharp2018
{
    /// <summary>
    /// Static class with icons (ImageSources). Works only on Android... For iOS can be done later - change the path
    /// </summary>
    public class Icons
    {
        public static ImageSource EmptyStarIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.emptystar.png");
        public static ImageSource FullStarIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.fullstar.png");
        public static ImageSource HomeIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.home.png");
        public static ImageSource ResizeIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.resize.png");


        public static ImageSource SaveIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.saveIcon.png");

        public static ImageSource GrammarIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.grammarIcon.png");
        public static ImageSource VocabularyIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.vocabularyIcon.png");
        public static ImageSource PracticeIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.practiceIcon.png");
        public static ImageSource PracticeWithoutIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.practiceYourselfIcon.png");

        public static ImageSource DeleteIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.deleteIcon.png");

        public static ImageSource DoneIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.DoneIcon.png");
        public static ImageSource LockedIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.LockedIcon.png");
        public static ImageSource WorkingIcon =
            ImageSource.FromResource("ZapoctakCSharp2018.Droid.Images.WorkingIcon.png");

        public static ImageSource GetIconByType(Type type)
        {
            if (TypeImageDict.ContainsKey(type))
            {
                return TypeImageDict[type];
            }

            return DoneIcon;
        }

        private static Dictionary<Type, ImageSource> TypeImageDict = new Dictionary<Type, ImageSource>()
        {
            { typeof(GrammarItem), GrammarIcon },
            { typeof(VocabularyItem), VocabularyIcon },
            { typeof(PracticeWithCheckItem), PracticeIcon },
            { typeof(PracticeWithNoCheckItem), PracticeWithoutIcon }
        };
    }
}
