﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018.AppView
{    
    /// <summary>
    /// View of certain lesson, shows list of LessonItems
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LessonPage : ContentPage
	{
		public LessonPage (LessonPageViewModel l)
		{        
            BindingContext = l;            
            InitializeComponent();            
        }                                  
    }        
}