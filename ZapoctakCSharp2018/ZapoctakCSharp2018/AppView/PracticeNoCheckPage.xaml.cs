﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZapoctakCSharp2018.AppView;
using ZapoctakCSharp2018.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018.AppView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PracticeNoCheckPage : ContentPage
	{
		public PracticeNoCheckPage (PracticeNoCheckViewModel practiceNoCheckViewModel)
		{
			InitializeComponent ();
            BindingContext = practiceNoCheckViewModel;            
		}
	}
}