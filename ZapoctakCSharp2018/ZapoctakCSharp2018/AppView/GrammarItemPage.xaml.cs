﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZapoctakCSharp2018.ViewModel;

namespace ZapoctakCSharp2018.AppView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GrammarItemPage : ContentPage
	{
		public GrammarItemPage (GrammarItemPageViewModel viewModel)
		{
			InitializeComponent ();
            BindingContext = viewModel;
		}

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }    
}