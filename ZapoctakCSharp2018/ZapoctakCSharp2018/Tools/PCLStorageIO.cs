﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform;
using Xamarin.Forms.Internals;
using ZapoctakCSharp2018.ViewModel;
using ZapoctakCSharp2018.AppView;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using PCLStorage;
using System.IO;
using System.Diagnostics;

namespace ZapoctakCSharp2018
{
    public class PCLStorageIO
    {
        // generic method for saving (serializing) objects
        public static async Task SaveUserFile<T>(T fileToSave, string folderName, string nameOfFile)
        {            
            // get folder with user files
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder userFolder;
            userFolder = await rootFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);

            // get needed file according to nameOfFile
            IFile userFile;
            userFile = await userFolder.CreateFileAsync(nameOfFile, CreationCollisionOption.ReplaceExisting);

            // serializer of given type T
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, fileToSave);                
                await userFile.WriteAllTextAsync(textWriter.ToString());
            }
        }

        /// <summary>
        /// Async generic method for loading (deserializing) objects        
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Returns default(T) if anything fails</returns>
        public static async Task<T> LoadUserFile<T>(string folderName, string nameOfFile)
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            // try to find folder with user info
            ExistenceCheckResult existsFolder = await rootFolder.CheckExistsAsync(folderName).ConfigureAwait(false);

            // the folder doesn't exist -> the program runs for the first time -> return empty progress
            if (existsFolder == ExistenceCheckResult.NotFound)
            {            
                return default(T);
            }
            
            // the folder exists
            IFolder folder = await rootFolder.GetFolderAsync(folderName).ConfigureAwait(false);

            // check if the file with progress exists
            ExistenceCheckResult existsFile = await folder.CheckExistsAsync(nameOfFile).ConfigureAwait(false);
            if (existsFile == ExistenceCheckResult.NotFound)
            {            
                return default(T);
            }
            
            // the file exists
            IFile userFile = await folder.GetFileAsync(nameOfFile).ConfigureAwait(false);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            string fileContent = await userFile.ReadAllTextAsync().ConfigureAwait(false);
            if (fileContent == null || fileContent == "")
            {
                return default(T);
            }

            using (TextReader textReader = new StringReader(fileContent))
            {
                T up = (T)xmlSerializer.Deserialize(textReader);
                return up;
            }
        }
    }
}
