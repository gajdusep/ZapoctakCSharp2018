﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ZapoctakCSharp2018
{
    public class ColorTools
    {
        public static List<Color> staticColorList = new List<Color>
        {
            Color.DarkGreen,Color.DarkBlue, Color.DarkOliveGreen, Color.DarkKhaki,Color.DarkGoldenrod
        };

        public static Color RandomColor()
        {
            Random rnd = new Random();
            Color cl = new Color(rnd.NextDouble(), rnd.NextDouble(), rnd.Next());
            return cl;
        }

        public static List<Color> GetColors(int k)
        {
            List<Color> list = new List<Color>();
            if (k < 1)
            {
                throw new IndexOutOfRangeException("k must be at least 1");
            }

            for (int i = 0; i < Math.Min(k, staticColorList.Count); i++)
            {
                list.Add(staticColorList[i]);
            }
            if (k > staticColorList.Count)
            {
                for (int i = staticColorList.Count; i < k; i++)
                {
                    list.Add(RandomColor());
                }
            }
            return list;
        }

        private static Dictionary<Type, Color> LessonColorDict = new Dictionary<Type, Color>
        {
            { typeof(GrammarItem), Color.LightGreen },
            { typeof(VocabularyItem), Color.LightSkyBlue },
            { typeof(PracticeWithCheckItem), Color.LightSeaGreen },
            { typeof(PracticeWithNoCheckItem), Color.BlueViolet }
        };

        public static Color GetLessonItemColor(Type type)
        {
            Color color;
            if (!LessonColorDict.TryGetValue(type, out color))
            {
                color = Color.Yellow;
            }
            return color;
        }
    }
}
