﻿using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ZapoctakCSharp2018
{
    /// <summary>
    /// Class for reading prepared data, that aren't changed with user's actions.
    /// Suitable for reading all book-data classes (Book, Lesson, LessonItems, LessonContents...)
    /// </summary>
    public class XMLDataIO
    {        
        public static string BookPath = "Book.xml";
                 
        public static T LoadXmlData<T>(string filename)
        {            
#if __IOS__
            var resourcePrefix = "ZapoctakCSharp2018.iOS.";

#elif __ANDROID__
            var resourcePrefix = "ZapoctakCSharp2018.Droid.";
#else
            var resourcePrefix = "";
#endif
            string folderName = "ExampleXMLFiles.";
            
            T myobject;
            
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(XMLDataIO)).Assembly;
            Stream stream = assembly.GetManifestResourceStream(resourcePrefix + folderName + filename);
            
            using (var reader = new StreamReader(stream))
            {
                var serializer = new XmlSerializer(typeof(T));
                myobject = (T)serializer.Deserialize(reader);
                reader.Close();
            }
            
            return myobject;            
        }
    }
}
