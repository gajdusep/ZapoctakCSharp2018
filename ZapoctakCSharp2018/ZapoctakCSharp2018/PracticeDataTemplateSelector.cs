﻿using ZapoctakCSharp2018.ViewModel;
using Xamarin.Forms;

namespace ZapoctakCSharp2018
{
    public class PracticeDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate AnswerDataTemplate { get; set; }        
        public DataTemplate TranslateItDataTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is WordAnswerViewModel viewModel)
            {
                if (viewModel.ResultCorrect == null)
                {
                    return TranslateItDataTemplate;
                }
                else
                {
                    return AnswerDataTemplate;
                }                
            }
            return new DataTemplate();
        }
    }
}
