﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Windows.Input;
using System.Diagnostics;
using System.Threading.Tasks;
using ZapoctakCSharp2018.AppView;
using System.Linq;
using System.Collections.ObjectModel;

namespace ZapoctakCSharp2018.ViewModel
{
    public class LessonPageViewModel : BaseViewModel
    {
        public INavigation Navigation { get; set; }

        /// <summary>
        /// Prepares TupleList for good binding and coordination with View
        /// </summary>
        /// <param name="lessonItems">Lesson items of current Lesson</param>
        /// <param name="itemsProgress">Lesson items' progress of current Lesson</param>
        private void PrepareTupleList(IEnumerable<LessonItem> lessonItems, LessonProgress lessonProgress)
        {
            tupleList = new CustomObservableCollection<ItemProgressViewModel>();

            if (lessonProgress.ItemsProgress == null || lessonProgress.ItemsProgress.Count == 0)
            {
                lessonProgress.ItemsProgress = new ObservableCollection<ItemProgress>();
                foreach (var lessonItem in lessonItems)
                {
                    ItemProgress itemProgress = new ItemProgress() { Name = lessonItem.Name };
                    lessonProgress.ItemsProgress.Add(itemProgress);
                    tupleList.Add(new ItemProgressViewModel(lessonItem, itemProgress, LessonViewModel.UpdateLessonProgress));
                }                
            }
            else
            {
                foreach (var lessonItem in lessonItems)
                {
                    var results = from prog in lessonProgress.ItemsProgress
                                  where prog.Name == lessonItem.Name
                                  select prog;
                    var resList = results.ToList();
                    ItemProgress itemProgress = new ItemProgress(); // only preparing
                    if (resList.Count == 0)
                    {
                        itemProgress = new ItemProgress()
                        {
                            Name = lessonItem.Name
                        };
                        lessonProgress.ItemsProgress.Add(itemProgress);
                    }
                    else if (resList.Count == 1)
                    {
                        itemProgress = resList[0];
                    }
                    else
                    {
                        // two or more results - shoudln't happen...
                        itemProgress = resList[0];
                    }

                    tupleList.Add(new ItemProgressViewModel(lessonItem, itemProgress, LessonViewModel.UpdateLessonProgress));
                }
            }
            if (TupleList.Count > 0 && tupleList[0].Progress.ActualProgress < 0)
            {
                tupleList[0].Progress.ActualProgress = 0;
                tupleList[0].ActStateOfItem = ProgressState.Working;
            }                        
        }

        public LessonPageViewModel(Lesson lesson, INavigation navigation, LessonProgress lessonProgress, LessonViewModel viewModel)
        {
            Navigation = navigation;
            this.lesson = lesson;

            LessonViewModel = viewModel;
            // LessonListPageViewModel = listViewModel;

            PrepareTupleList(lesson.LessonItems, lessonProgress);

            LessonItemClickedCommand = new Command(async (object o) => await ListView_ItemSelected(o));
        }
        
        private LessonViewModel LessonViewModel;

        public ICommand LessonItemClickedCommand { get; private set; }
        
        private async Task ListView_ItemSelected(object o)
        {
            if (o == null) return;
                        
            ItemProgressViewModel ipvm = ((ItemProgressViewModel)o); 
            ItemProgress progress = ipvm.Progress;
            ProgressState state = ipvm.ActStateOfItem;

            if (state != ProgressState.Locked)
            {
                var item = ipvm.Item;

                if (item is GrammarItem grammarItem)
                {
                    await PushGrammar(grammarItem, progress, ipvm);
                }
                else if (item is VocabularyItem vocabularyItem)
                {
                    await PushVocabulary(vocabularyItem, progress, ipvm);
                }
                else if (item is PracticeWithCheckItem withCheck)
                {
                    await PushCheckPractice(withCheck, progress, ipvm);
                }
                else if (item is PracticeWithNoCheckItem withoutCheck)
                {
                    await PushNoCheckPractice(withoutCheck, progress, ipvm);
                }                
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Zamčeno", "První dokončete předchozí cvičení", "OK");
            }            
        }
        
        private async Task PushGrammar(GrammarItem grammarItem, ItemProgress progress, ItemProgressViewModel ipvm)
        {
            if (grammarItem.LessonContents == null || grammarItem.LessonContents.Count == 0)
            {
                var v = new GrammarItem();
                // Try read file with item
                try
                {
                    await Task.Run(() => { v = XMLDataIO.LoadXmlData<GrammarItem>(grammarItem.Path); });
                    grammarItem.LessonContents = v.LessonContents;
                }
                catch (System.Exception)
                {
                    Debug.Write("Something wrong with reading xml files");
                }
            }

            GrammarItemPageViewModel viewModel = new GrammarItemPageViewModel(grammarItem, progress, Navigation, ipvm.SetActualProgress);
            await Navigation.PushAsync(new GrammarItemPage(viewModel));
        }

        private async Task PushVocabulary(VocabularyItem vocabularyItem, ItemProgress progress, ItemProgressViewModel ipvm)
        {
            if (vocabularyItem.Vocabulary == null || vocabularyItem.Vocabulary.Count == 0)
            {
                VocabularyItem v = new VocabularyItem();
                try
                {
                    await Task.Run(() => { v = XMLDataIO.LoadXmlData<VocabularyItem>(vocabularyItem.Path); });
                    vocabularyItem.Vocabulary = v.Vocabulary;
                }
                catch (System.Exception)
                {
                    Debug.Write("Something wrong with reading xml files");
                }
            }

            VocabularyItemPageViewModel viewModel = new VocabularyItemPageViewModel(vocabularyItem, progress, Navigation, ipvm.SetActualProgress);
            await Navigation.PushAsync(new VocabularyItemPageView(viewModel));
        }

        private async Task PushCheckPractice(PracticeWithCheckItem withCheck, ItemProgress progress, ItemProgressViewModel ipvm)
        {
            if (withCheck.ListToTranslate == null || withCheck.ListToTranslate.Count == 0)
            {
                PracticeWithCheckItem p = new PracticeWithCheckItem();
                try
                {
                    await Task.Run(() => { p = XMLDataIO.LoadXmlData<PracticeWithCheckItem>(withCheck.Path); });
                    withCheck.ListToTranslate = p.ListToTranslate;
                    withCheck.WhatToDo = p.WhatToDo;
                }
                catch (System.Exception)
                {
                    Debug.Write("Something wrong with reading xml files");
                }
            }

            PracticeCarouselPageViewModel viewModel = new PracticeCarouselPageViewModel(withCheck, progress, Navigation, ipvm.SetActualProgress);
            await Navigation.PushAsync(new PracticeCarouselPage(viewModel));
        }

        private async Task PushNoCheckPractice(PracticeWithNoCheckItem withoutCheck, ItemProgress progress, ItemProgressViewModel ipvm)
        {
            if (withoutCheck.LessonContents == null || withoutCheck.LessonContents.Count == 0)
            {
                var v = new PracticeWithNoCheckItem();
                // Try read file with item
                try
                {
                    await Task.Run(() => { v = XMLDataIO.LoadXmlData<PracticeWithNoCheckItem>(withoutCheck.Path); });
                    withoutCheck.LessonContents = v.LessonContents;
                }
                catch (System.Exception)
                {
                    Debug.Write("Something wrong with reading xml files");
                }
            }

            string pathToAnswers = "Answers." + withoutCheck.Path;

            PracticeNoCheckViewModel viewModel = new PracticeNoCheckViewModel(withoutCheck, progress, Navigation, ipvm.SetActualProgress);
            await Navigation.PushAsync(new PracticeNoCheckPage(viewModel));
        }

        // using CustomObservableCollection - it updates GUI is any of items is updated 
        //   (normal ObservableCollection doesn't have this behaviour
        private CustomObservableCollection<ItemProgressViewModel> tupleList;
        public CustomObservableCollection<ItemProgressViewModel> TupleList
        {
            get { return tupleList; }
            set { SetProperty(ref tupleList, value); }
        }

        private Lesson lesson;
        public Lesson Lesson
        {
            get { return lesson; }
            set { SetProperty(ref lesson, value); }
        }
    }
}
