﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ZapoctakCSharp2018.ViewModel
{
    public class PracticeCarouselPageViewModel : FavouritesAddableBaseViewModel
    {
        // INavigation is needed for comfortable working with Commands
        private INavigation navi;          

        // constans for description of another action after clicks (defautly in Czech)
        readonly static string checkButtonText = "Zkontroluj";
        readonly static string wrongAnswerButtonText = "Chápu, dále";
        readonly static string correctAnswerButtonText = "Pokračovat";
        // readonly static string endOfPractice = "Ukončit cvičení";

        private static readonly string correctAnswerText = "Správně!";
        private static readonly string wrongAnswerText = "Špatně!";
        private static readonly Color correctColor = Color.Green;
        private static readonly Color wrongColor = Color.Red;

        // the previous static strings are assigned to ButtonText
        private string buttonText;
        public string ButtonText
        {
            get { return buttonText; }
            set { SetProperty(ref buttonText, value); }
        }

        private string answerCorrectnessText;
        public string AnswerCorrectnessText
        {
            get { return answerCorrectnessText; }
            set { SetProperty(ref answerCorrectnessText, value); }
        }

        private Color answerColor;
        public Color AnswerColor
        {
            get { return answerColor; }
            set { SetProperty(ref answerColor, value); }
        }

        // sets all variables that are describing the corectness of the answer
        private void SetTextAndColor(bool correct)
        {
            if (correct)
            {
                AnswerColor = correctColor;
                AnswerCorrectnessText = correctAnswerText;
                ButtonText = correctAnswerButtonText;
            }
            else
            {
                AnswerColor = wrongColor;
                AnswerCorrectnessText = wrongAnswerText;
                ButtonText = wrongAnswerButtonText;
            }
        }

        // description of practice (for example: "fill in the gaps")
        private string whatToDo;
        public string WhatToDo
        {
            get { return whatToDo; }
            set { SetProperty(ref whatToDo, value); }
        }

        // Queue for words (sentences) to practise; if the answer is wrong, the words goes to the end of the queue
        private Queue<WordAnswerViewModel> queue;

        // Collection for CarouselView, in this collection is always only one item (for CarouselView)
        private CustomObservableCollection<WordAnswerViewModel> tupleWordAnswerObservable;
        public CustomObservableCollection<WordAnswerViewModel> TupleWordAnswerObservable
        {
            get { return tupleWordAnswerObservable; }
            set { SetProperty(ref tupleWordAnswerObservable, value); }
        }

        // Command for dealing with user's answers
        private ICommand next;
        public ICommand Next
        {
            get { return next; }
            set { SetProperty(ref next, value); }
        }

        // Progress, that will be changed after the practice is done
        private ItemProgress itemProgress;
        
        // For saving favourite words
        private FavouritesPageViewModel favourites;

        // For update of progress
        private Action itemUpdateAction;

        public PracticeCarouselPageViewModel(PracticeWithCheckItem practiceWithCheckItem, ItemProgress progress, INavigation navigation, Action updateItemProgressAction = null)
        {                  
            // sentence about the purpose of current practice
            WhatToDo = practiceWithCheckItem.WhatToDo;
            
            itemProgress = progress;
            navi = navigation;
            itemUpdateAction = updateItemProgressAction;
            queue = new Queue<WordAnswerViewModel>();

            // make the queue of all words in practice item
            var words = practiceWithCheckItem.ListToTranslate;
            foreach (var word in words)
            {
                queue.Enqueue(new WordAnswerViewModel
                {
                    Answer = "",
                    Word = word
                });
            }

            // put first word into the collection (content of carousel view)
            tupleWordAnswerObservable = new CustomObservableCollection<WordAnswerViewModel>
            {
                queue.Dequeue()
            };

            buttonText = checkButtonText;
            Next = new Command(async (object carousel) => await CheckAndMove((CarouselView)carousel));
            StarClickedCommand = new Command(async () => await StarClicked());

            if (Application.Current.Properties.ContainsKey("Favourites"))
            {
                favourites = Application.Current.Properties["Favourites"] as FavouritesPageViewModel;                
            }

            if (favourites.WordAlreadyInCollection(tupleWordAnswerObservable[0].Word.OriginalWord))
            {
                StarIconPath = fullStarPath;
            }
            else
            {
                StarIconPath = emptyStarPath;
            }                  
        }
                       
        /// <summary>
        /// Changes the state of starIcon. Calls AddWord or DeleteItem methods in favourites.
        /// </summary>
        /// <returns></returns>
        protected override async Task StarClicked()
        {
            if (StarIconPath == emptyStarPath)
            {
                StarIconPath = fullStarPath;                                
                await Task.Run(() => favourites.AddWord(tupleWordAnswerObservable[0].Word));
            }
            else
            {
                StarIconPath = emptyStarPath;                
                await Task.Run(() => favourites.DeleteItem(tupleWordAnswerObservable[0].Word));                
            }
        }

        /// <summary>
        /// Checks the user's answer and depending on it decides what next.
        /// </summary>
        /// <param name="carousel"></param>
        /// <returns></returns>
        private async Task CheckAndMove(CarouselView carousel)
        {                        
            // in tupleWordAnswerObservable is always only one word
            WordAnswerViewModel tupleWordAnswer = tupleWordAnswerObservable[0];
            
            var valTuple = AnswersChecker.VocabularyChecker(tupleWordAnswer.Word, tupleWordAnswer.Answer, false);
            bool condition = valTuple.Item2 < 3;
            
            // the word wasn't checked, so check it and set button text
            if (tupleWordAnswer.ResultCorrect == null)
            {                
                tupleWordAnswer.ResultCorrect = condition;
                SetTextAndColor(condition);
            }
            // the word was checked before
            else
            {
                starIconPath = emptyStarPath;

                tupleWordAnswer.ResultCorrect = null;
                ButtonText = checkButtonText;

                if (condition)
                {
                    if (queue.Count > 0)
                    {
                        TupleWordAnswerObservable[0] = queue.Dequeue();
                    }
                    // end of queue to translate
                    else
                    {
                        itemProgress.ActualProgress = itemProgress.MaximumProgress;
                        itemUpdateAction?.Invoke();
                        await navi.PopAsync();                        
                    }
                }
                else
                {
                    tupleWordAnswer.Answer = "";
                    queue.Enqueue(tupleWordAnswer);
                    TupleWordAnswerObservable[0] = queue.Dequeue();
                }
            }

            // workaround to update GUI
            carousel.ItemsSource = null;            
            carousel.ItemsSource = TupleWordAnswerObservable;
        }                                
    }
}
