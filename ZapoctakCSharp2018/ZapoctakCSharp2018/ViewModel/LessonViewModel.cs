﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;

namespace ZapoctakCSharp2018.ViewModel
{
    // Helping viewModel class. Used in LessonListPage as a ListView item.
    public class LessonViewModel : ProgressBaseViewModel
    {                
        public LessonViewModel(Lesson lesson, LessonProgress lessonProgress, INavigation navigation, Action updateLessonListAction)
        {
            this.lesson = lesson;
            this.lessonProgress = lessonProgress;
            this.Navigation = navigation;            
            this.updateLessonListAction = updateLessonListAction;

            ShowLessonItems = new Command(async (object item) =>
            {
                if (item is LessonViewModel viewM)
                {
                    await ShowLessOrMore(viewM);
                }
            });                       
        }

        // Command for showing the content of a lesson.
        private ICommand showLessonItems;
        public ICommand ShowLessonItems
        {
            get { return showLessonItems; }
            set { SetProperty(ref showLessonItems, value); }
        }

        private INavigation Navigation { get; set; }

        // method for showing the contents of the lesson - it is allowed to look even the lesson itself is closed yet
        private async Task ShowLessOrMore(LessonViewModel viewModel)
        {
            Lesson lesson = viewModel.Lesson;
            if (lesson.LessonItems == null || lesson.LessonItems.Count == 0)
            {
                Lesson helpLes = new Lesson();
                await Task.Run(() => { helpLes = XMLDataIO.LoadXmlData<Lesson>(lesson.Path); });
                lesson.LessonItems = helpLes.LessonItems;
            }
            IsVisible = !IsVisible;
        }        

        public ProgressState ActStateOfLesson
        {
            get { return actualProgressState; }
            set { SetProperty(ref actualProgressState, value); }
        }

        private bool isVisible;
        public bool IsVisible
        {
            get { return isVisible; }
            set { SetProperty(ref isVisible, value); }
        }

        // method for updating the progress
        public void UpdateLessonProgress()
        {
            // go through the lessonitems. If any item is Done, make the next one either Working or let it remain Done (if it's done already)
            for (int i = 0; i < lessonPageViewModel.TupleList.Count - 1; i++)
            {
                if (lessonPageViewModel.TupleList[i].ActStateOfItem == ProgressState.Done)
                {
                    if (lessonPageViewModel.TupleList[i + 1].ActStateOfItem == ProgressState.Locked)
                    {
                        lessonPageViewModel.TupleList[i + 1].ActStateOfItem = ProgressState.Working;
                    }
                }
            }
            if (LessonPageViewModel.TupleList[LessonPageViewModel.TupleList.Count - 1].ActStateOfItem == ProgressState.Done)
            {
                ActStateOfLesson = ProgressState.Done;
            }

            // because of GUI - update the list of lessons (the change of the state of one lesson may open another lesson)
            updateLessonListAction?.Invoke();
        }

        private Action updateLessonListAction;

        private LessonPageViewModel lessonPageViewModel;
        public LessonPageViewModel LessonPageViewModel
        {
            get { return lessonPageViewModel; }
            set { SetProperty(ref lessonPageViewModel, value); }
        }

        private Lesson lesson;
        public Lesson Lesson
        {
            get { return lesson; }
            set { SetProperty(ref lesson, value); }
        }

        private LessonProgress lessonProgress;
        public LessonProgress LessonProgress
        {
            get { return lessonProgress; }
            set { SetProperty(ref lessonProgress, value); }
        }
    }
}
