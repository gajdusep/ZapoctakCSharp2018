﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Windows.Input;
using ZapoctakCSharp2018.AppView;
using System.Linq;
using System.Threading.Tasks;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// ViewModel for page with list of saved favourites words
    /// </summary>
    public class FavouritesPageViewModel : BaseViewModel
    {        
        public FavouritesPageViewModel(Favourites favourites, INavigation navigation)
        {
            favouriteWords = favourites.FavouriteWords;
            navi = navigation;

            deleteItemCommand = new Command(async (object o) => 
            {
                string deleteQuestion = "Opravdu chcete odstranit toto slovo z oblíbených?";
                bool answer = await App.Current.MainPage.DisplayAlert("Odstranit?", deleteQuestion, "Ano", "Ne");
                if (answer)
                {
                    await DeleteItem((Word)o);
                }                
            });

            PracticeFavourites = new Command(async () => await OpenPractice());
        }

        public ImageSource DeleteIcon { get; set; } = Icons.DeleteIcon;        

        public string PracticeText { get; private set; } = "Procvičit";
        public string FavouritesName { get; private set; } = "Oblíbená slovíčka";


        public void DeleteItem(string originalWord)
        {
            List<int> indexes = new List<int>();
            for (int i = 0; i < FavouriteWords.Count; i++)
            {
                if (FavouriteWords[i].OriginalWord == originalWord)
                {
                    indexes.Add(i);
                }
            }
            for (int i = indexes.Count - 1; i >= 0; i--)
            {
                FavouriteWords.RemoveAt(indexes[i]);
            }
        }

        public async Task DeleteItem(Word word)
        {
            await Task.Run(() => DeleteItem(word.OriginalWord));
        }

        public bool WordAlreadyInCollection(string s)
        {
            if (favouriteWords == null)
            {
                return false;
            }
            foreach (var word in favouriteWords)
            {
                if (word.OriginalWord == s)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddWord(Word word)
        {
            if (favouriteWords == null)
            {
                favouriteWords = new CustomObservableCollection<Word>();
            }
            if (!WordAlreadyInCollection(word.OriginalWord))
            {
                favouriteWords.Add(word);
            }
        }

        public ICommand PracticeFavourites { get; set; }

        private INavigation navi;

        int numberToPractice = 15;
        static List<string> alert = new List<string>() { "Nic na procvičení", "Seznam oblíbených je prázdný.", "ok :(" };
        private async Task OpenPractice()
        {
            if (favouriteWords == null || favouriteWords.Count == 0)
            {
                await App.Current.MainPage.DisplayAlert(alert[0], alert[1], alert[2]);
                return;
            }

            PracticeWithCheckItem practiceWithCheckItem = new PracticeWithCheckItem
            {
                WhatToDo = "Přeložte"
            };
            int min = favouriteWords.Count < numberToPractice ? favouriteWords.Count : numberToPractice;
            var randomShuffled = favouriteWords.OrderBy(a => Guid.NewGuid()).ToList();
            practiceWithCheckItem.ListToTranslate = new List<Word>();
            for (int i = 0; i < min; i++)
            {
                practiceWithCheckItem.ListToTranslate.Add(randomShuffled[i]);
            }

            ItemProgress itemProgress = new ItemProgress();
            PracticeCarouselPageViewModel viewModel = new PracticeCarouselPageViewModel(practiceWithCheckItem, itemProgress, navi);

            await navi.PushAsync(new PracticeCarouselPage(viewModel));
        }

        private ICommand deleteItemCommand;
        public ICommand DeleteItemCommand
        {
            get { return deleteItemCommand; }
            set { SetProperty(ref deleteItemCommand, value); }
        }

        private CustomObservableCollection<Word> favouriteWords;
        public CustomObservableCollection<Word> FavouriteWords
        {
            get { return favouriteWords; }
            set { SetProperty(ref favouriteWords, value); }
        }
    }
}
