﻿using System;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZapoctakCSharp2018.AppView;

namespace ZapoctakCSharp2018.ViewModel
{
    public class MainPageViewModel : BaseViewModel
    {        
        // CONSTRUCTOR
        public MainPageViewModel(INavigation navigation, UserProgress userProgress)
        {
            this.Navigation = navigation;
            Book = null;
            this.userProgress = userProgress;
            LoadBookCommand = new Command(async () => await LoadBookAsync());
            FavouritesCommand = new Command(async () => await GoToFavourites());
        }

        private UserProgress userProgress;
        public UserProgress UserProgress
        {
            get { return userProgress; }
            set { SetProperty(ref userProgress, value); }
        }

        private static readonly string lessonsButtonText = "LEKCE";
        private static readonly string favouritesButtonText = "ULOŽENÁ SLOVÍČKA";

        public string LessonsText { get; private set; } = lessonsButtonText;
        public string FavouritesText { get; private set; } = favouritesButtonText;        
                
        private ICommand favouritesCommand;
        public ICommand FavouritesCommand
        {
            get { return favouritesCommand; }
            set { SetProperty(ref favouritesCommand, value); }
        }       

        private ICommand loadBookCommand;
        public ICommand LoadBookCommand
        {
            get { return loadBookCommand; }
            set { SetProperty(ref loadBookCommand, value); }
        }

        // Loads the Book object from the file (only list of lessons) and pushes the LessonListPage
        private async Task LoadBookAsync()
        {
            Book actBook = Book;

            if (actBook == null)
            {
                await Task.Run(() => { actBook = XMLDataIO.LoadXmlData<Book>("Book.xml"); });
            }

            Book = actBook;

            await Navigation.PushAsync(new LessonListView(new LessonListPageViewModel(Book, UserProgress, Navigation)));
        }

        Book book;
        public Book Book
        {
            get { return book; }
            set { SetProperty(ref book, value); }
        }

        public async Task GoToFavourites()
        {
            var favourites = App.Current.Properties["Favourites"] as FavouritesPageViewModel;
            await Navigation.PushAsync(new FavouritesPageView(favourites));
        }

        public INavigation Navigation { get; set; }


    }
}
