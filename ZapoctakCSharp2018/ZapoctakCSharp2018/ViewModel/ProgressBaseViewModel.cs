﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ZapoctakCSharp2018.ViewModel
{
    public enum ProgressState { Done, Working, Locked };

    /// <summary>
    /// Parent of progress ViewModels in this solution. 
    /// It puts together some instance (e.g. LessonItem) with its progress (e.g. LessonItemProgress)
    /// It holds private ProgressState actualProgressState, the ProgressState property depends on further implementation.
    /// It holds ImageSource property that depends on actualProgressState and returns prepared static ImageSources
    /// </summary>
    public abstract class ProgressBaseViewModel : BaseViewModel
    {
        protected ProgressState actualProgressState;

        public ImageSource ImageSource
        {
            get
            {
                switch (actualProgressState)
                {
                    case ProgressState.Done:
                        return doneImageSource;
                    case ProgressState.Working:
                        return workingImageSource;
                    case ProgressState.Locked:
                        return lockedImageSource;
                    default:
                        return workingImageSource;
                }
            }
        }

        private static ImageSource doneImageSource = Icons.DoneIcon;
        private static ImageSource lockedImageSource = Icons.LockedIcon;
        private static ImageSource workingImageSource = Icons.WorkingIcon;
    }
}
