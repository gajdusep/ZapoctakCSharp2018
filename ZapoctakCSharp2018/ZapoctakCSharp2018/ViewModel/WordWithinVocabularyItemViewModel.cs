﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// Helping class for binding Words in VocabularyItem.
    /// Inherits from FavouritesAddableBaseViewModel -> word can be added to favourite words.
    /// </summary>
    public class WordWithinVocabularyItemViewModel : FavouritesAddableBaseViewModel
    {
        public WordWithinVocabularyItemViewModel(Word word)
        {
            this.word = word;
            favs = App.Current.Properties["Favourites"] as FavouritesPageViewModel;
                                
            if (favs.WordAlreadyInCollection(word.OriginalWord))
            {
                StarIconPath = fullStarPath;
            }
            else
            {
                StarIconPath = emptyStarPath;
            }
            
            StarClickedCommand = new Command(async (object o) => { await StarClicked(); });
        }

        private FavouritesPageViewModel favs;

        private Word word;
        public Word Word
        {
            get { return word; }
            set { SetProperty(ref word, value); }
        }

        protected async override Task StarClicked()
        {            
            if (StarIconPath == fullStarPath)
            {                    
                StarIconPath = emptyStarPath;                
                favs.DeleteItem(Word.OriginalWord);
            }
            else
            {
                StarIconPath = fullStarPath;                
                favs.AddWord(word);
            }                                    
        }
    }
}
