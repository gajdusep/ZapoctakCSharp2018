﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ZapoctakCSharp2018.ViewModel
{    
    /// <summary>
    /// ViewModel of which every ViewModel in my solution inherits.
    /// In every ViewModel you can define classic bindable property as follows:
    ///     
    /// private SomeType privateMember;
    /// public SomeType PublicProperty
    /// { 
    ///     get { return privateMember; }
    ///     set { SetProperty(privateMember, value); }
    /// 
    /// }    
    /// </summary>
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs((propertyName)));
        }
                
        protected bool SetProperty<T>(ref T storage, T value, string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
            {
                return false;
            }
            storage = value;
            OnPropertyChanged(propertyName);

            return true;
        }
    }
}
