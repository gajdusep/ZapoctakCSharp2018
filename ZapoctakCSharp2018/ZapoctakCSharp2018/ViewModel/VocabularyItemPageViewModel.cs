﻿using System;
using System.Collections.Generic;
using System.Text;
using ZapoctakCSharp2018.AppView;
using System.Windows.Input;
using Xamarin.Forms;

namespace ZapoctakCSharp2018.ViewModel
{
    public class VocabularyItemPageViewModel : BaseViewModel
    {
        public VocabularyItemPageViewModel(VocabularyItem vocabularyItem, ItemProgress itemProgress, INavigation navigation, Action updateItemProgressAction = null)
        {
            // prepare vocabulary list
            words = new CustomObservableCollection<WordWithinVocabularyItemViewModel>();
            foreach (var w in vocabularyItem.Vocabulary)
            {
                words.Add(new WordWithinVocabularyItemViewModel(w));
            }
            
            ItemProgress = itemProgress;
            PageName = vocabularyItem.Name;

            Navigation = navigation;
            EndItemCommand = new Command(async () =>
            {
                ItemProgress.ActualProgress = ItemProgress.MaximumProgress;
                updateItemProgressAction?.Invoke();
                await Navigation.PopAsync();
            });
        }

        private ItemProgress ItemProgress { get; set; }

        public string PageName { get; private set; }
        
        public string EndItemText { get{ return "Přečteno, ukonči"; } }

        public INavigation Navigation;        
        public ICommand EndItemCommand { get; private set; }

        private CustomObservableCollection<WordWithinVocabularyItemViewModel> words;
        public CustomObservableCollection<WordWithinVocabularyItemViewModel> Words
        {
            get { return words; }
            set { SetProperty(ref words, value); }
        }
    }
}
