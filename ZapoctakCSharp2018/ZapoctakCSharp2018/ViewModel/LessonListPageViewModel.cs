﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Linq;
using ZapoctakCSharp2018;
using ZapoctakCSharp2018.AppView;
using System.Diagnostics;
using System.Windows.Input;
using System.Threading.Tasks;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// ViewModel of LessonListView, contains list of Lessons and Commands for ListItem clicking
    /// </summary>
    public class LessonListPageViewModel : BaseViewModel
    {       
        public ICommand HomeCommand { get; set; }

        public ImageSource ResizeIcon { get; private set; } = Icons.ResizeIcon;

        private void PrepareLessons(Book book, UserProgress userProgress)
        {
            if (userProgress == null)
            {                
                throw new Exception("The user progress is either null, or LessonsProgress is null or empty");
            }
            if (userProgress.LessonsProgress == null || userProgress.LessonsProgress.Count == 0)
            {
                userProgress.LessonsProgress = new List<LessonProgress>();
                foreach (var les in book.Lessons)
                {
                    var prog = new LessonProgress()
                    {
                        Name = les.LessonName
                    };
                    userProgress.LessonsProgress.Add(prog);


                    LessonViewModel lvm = new LessonViewModel(les, prog, Navigation, UpdateLessonList);
                    lvm.ActStateOfLesson = ProgressState.Locked;
                    lessons.Add(lvm);
                }
            }
            else
            {
                foreach (var les in book.Lessons)
                {
                    var col = from lessProg in userProgress.LessonsProgress
                              where lessProg.Name == les.LessonName
                              select lessProg;

                    // the collection shoudln't be big, we can convert to list
                    var res = col.ToList();

                    LessonProgress lessonProgress;
                    if (res.Count == 0)
                    {
                        lessonProgress = new LessonProgress { Name = les.LessonName };
                        userProgress.LessonsProgress.Add(lessonProgress);
                    }
                    else if (res.Count == 1)
                    {
                        lessonProgress = res[0];
                    }
                    else
                    {
                        // two or more progresses for one lesson
                        lessonProgress = res[0];
                    }

                    LessonViewModel lvm = new LessonViewModel(les, lessonProgress, Navigation, UpdateLessonList);
                    if (lessonProgress.ItemsProgress == null || lessonProgress.ItemsProgress.Count == 0)
                    {
                        lvm.ActStateOfLesson = ProgressState.Locked;
                    }
                    else
                    {
                        if (lessonProgress.ItemsProgress.Last().ActualProgress < lessonProgress.ItemsProgress.Last().MaximumProgress)
                        {
                            lvm.ActStateOfLesson = ProgressState.Locked;
                        }
                        else
                        {
                            lvm.ActStateOfLesson = ProgressState.Done;
                        }
                    }                    

                    lessons.Add(lvm);                    
                }
            }

            if (lessons.First().ActStateOfLesson == ProgressState.Locked)
            {
                var first = lessons.First();
                if (first.LessonProgress.ItemsProgress == null || first.LessonProgress.ItemsProgress.Count == 0)
                {
                    lessons.First().ActStateOfLesson = ProgressState.Working;
                }
                else
                {
                    if (first.LessonProgress.ItemsProgress.Last().ActualProgress >= first.LessonProgress.ItemsProgress.Last().MaximumProgress)
                    {
                        Lessons.First().ActStateOfLesson = ProgressState.Done;
                    }
                    else
                    {
                        lessons.First().ActStateOfLesson = ProgressState.Working;
                    }
                }                
            }
            UpdateLessonList();
        }
        
        // updates the list of lessons progress
        public void UpdateLessonList()
        {
            for (int i = 0; i < lessons.Count - 1; i++)
            {
                if (lessons[i].ActStateOfLesson == ProgressState.Done)
                {
                    if (lessons[i + 1].ActStateOfLesson == ProgressState.Locked)
                    {
                        lessons[i + 1].ActStateOfLesson = ProgressState.Working;
                        break;
                    }                
                }
            }
        }

        public LessonListPageViewModel(Book book, UserProgress userProgress, INavigation navigation)
        {
            this.Navigation = navigation;
            this.book = book;

            HomeCommand = new Command(async () => { await Navigation.PopToRootAsync(); });
            lessonClickedCommand = new Command(async (object o) => await LessonSelected(o));

            // prepare observable collection of Lessons and info about them
            lessons = new CustomObservableCollection<LessonViewModel>();
            PrepareLessons(book, userProgress);
        }

        private ICommand lessonClickedCommand;
        public ICommand LessonClickedCommand
        {
            get { return lessonClickedCommand; }
            set { SetProperty(ref lessonClickedCommand, value); }
        }

        // method called when the lesson is clicked. If it's locked, an allert pops out, if working or done - the page is pushed
        async private Task LessonSelected(object o)
        {
            LessonViewModel lessonVM = (LessonViewModel)o;
            Lesson lesson = lessonVM.Lesson;
            LessonProgress lessonProgress = lessonVM.LessonProgress;            
            
            // LessonPageViewModel lessonPageViewModel = lessonVM.LessonPageViewModel;
            ProgressState state = lessonVM.ActStateOfLesson;

            // if the lesson wasn't loaded - read it from the files
            if (lesson.LessonItems == null || lesson.LessonItems.Count == 0)
            {
                Lesson helpLes = new Lesson();
                await Task.Run(() => { helpLes = XMLDataIO.LoadXmlData<Lesson>(lesson.Path); });
                lesson.LessonItems = helpLes.LessonItems;                
            }

            if (lessonVM.LessonPageViewModel == null)
            {
                lessonVM.LessonPageViewModel = new LessonPageViewModel(lesson, Navigation, lessonProgress, lessonVM);
            }
            
            if (state != ProgressState.Locked)
            {
                lessonVM.UpdateLessonProgress();
                await Navigation.PushAsync(new LessonPage(lessonVM.LessonPageViewModel));
            }
            else
            {                
                await App.Current.MainPage.DisplayAlert("Zamčeno", "První dokončete předchozí lekce", "OK");
            }            
        }

        // every lesson has its own LessonViewModel for easier manipulation
        private CustomObservableCollection<LessonViewModel> lessons;
        public CustomObservableCollection<LessonViewModel> Lessons
        {
            get { return lessons; }
            set { SetProperty(ref lessons, value); }
        }

        public INavigation Navigation { get; private set; }

        private Book book;
        public Book Book
        {
            get { return book; }
            set { SetProperty(ref book, value); }
        }
    }
}
