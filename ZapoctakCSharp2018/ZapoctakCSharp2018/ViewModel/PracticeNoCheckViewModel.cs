﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// ViewModel of practice without checking.
    /// </summary>
    public class PracticeNoCheckViewModel : BaseViewModel
    {
        public PracticeNoCheckViewModel(PracticeWithNoCheckItem practiceWithNoCheckItem, ItemProgress itemProgress, INavigation navigation, Action updateItemProgressAction = null)
        {
            contentAnswerTuples = new CustomObservableCollection<ContentAnswerTuple>();

            fileName = practiceWithNoCheckItem.Path;
            Task<List<string>> answerListTask = Task.Run(async () => await ReadAnswersFromFile());
            var answerList = answerListTask.Result;

            if (answerList == null || practiceWithNoCheckItem.LessonContents.Count != answerList.Count)
            {
                foreach (var content in practiceWithNoCheckItem.LessonContents)
                {                    
                    contentAnswerTuples.Add(new ContentAnswerTuple(content, ""));
                }
            }
            else
            {
                for (int i = 0; i < practiceWithNoCheckItem.LessonContents.Count; i++)
                {
                    contentAnswerTuples.Add(new ContentAnswerTuple(practiceWithNoCheckItem.LessonContents[i], answerList[i]));
                }
            }

            this.itemProgress = itemProgress;
            this.navigation = navigation;
            this.itemUpdateAction = updateItemProgressAction;            

            SaveAnswers = new Command(async () => await SaveAnswersTask());
        }

        private ItemProgress itemProgress;
        private Action itemUpdateAction;
        public ImageSource SaveIcon
        {
            get { return Icons.SaveIcon; }
        }
        

        private static string folderName = "PracticeAnswers";
        private string fileName;

        // saving answers (list of strings) to a file, name of file is the same as path to Item path
        private async Task WriteAnswersToFile()
        {
            var list = new List<string>();
            foreach (var item in ContentAnswerTuples)
            {
                list.Add(item.Answer);
            }
            await PCLStorageIO.SaveUserFile<List<string>>(list, folderName, fileName);
        }

        // reads already writen answers or returns null if there are no answers yet.
        private async Task<List<string>> ReadAnswersFromFile()
        {
            var list = await PCLStorageIO.LoadUserFile<List<string>>(folderName, fileName);
            return list;
        }

        private INavigation navigation;
        private static List<string> allertMessage = new List<string>{ "Nedokončeno", "Ve všech odpovědích musí být text. (Přetáhněte doleva pro další položky)", "OK" };

        // method called after the save button is clicked. Checks if all editors contain some text and saves the answers.
        private async Task SaveAnswersTask()
        {
            foreach (var item in contentAnswerTuples)
            {
                if (item.Answer == null || item.Answer == "")
                {
                    await App.Current.MainPage.DisplayAlert(allertMessage[0], allertMessage[1], allertMessage[2]);
                    return;
                }
            }

            await WriteAnswersToFile();
            itemProgress.ActualProgress = itemProgress.MaximumProgress;
            itemUpdateAction?.Invoke();                       
            await navigation.PopAsync();
        }

        private ICommand saveAnswers;
        public ICommand SaveAnswers
        {
            get { return saveAnswers; }
            set { SetProperty(ref saveAnswers, value); }
        }

        private CustomObservableCollection<ContentAnswerTuple> contentAnswerTuples;
        public CustomObservableCollection<ContentAnswerTuple> ContentAnswerTuples
        {
            get { return contentAnswerTuples; }
            set { SetProperty(ref contentAnswerTuples, value); }
        }
    }

    /// <summary>
    /// ViewModel class for easy binding in PracticeWithoutCheck
    /// </summary>
    public class ContentAnswerTuple : BaseViewModel
    {
        public ContentAnswerTuple(LessonContent content, string ans)
        {
            LessonContent = content;
            Answer = ans;
        }

        private LessonContent lessonContent;
        public LessonContent LessonContent
        {
            get { return lessonContent; }
            set { SetProperty(ref lessonContent, value); }
        }

        private string answer;
        public string Answer
        {
            get { return answer; }
            set { SetProperty(ref answer, value); }
        }
    }
}
