﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;

namespace ZapoctakCSharp2018.ViewModel
{
    public class GrammarItemPageViewModel : BaseViewModel
    {
        public GrammarItemPageViewModel(GrammarItem item, ItemProgress progress, INavigation navigation, Action updateItemProgressAction = null)
        {
            Navigation = navigation;            
            contents = item.LessonContents;
            ItemName = item.Name;
            ItemProgress = progress;
            this.updateItemProgressAction = updateItemProgressAction;

            EndItemText = "Přečteno, ukončit";
            EndItemCommand = new Command(async () => await EndItem());
        }

        private Action updateItemProgressAction;
        private async Task EndItem()
        {
            ItemProgress.ActualProgress = ItemProgress.MaximumProgress;
            updateItemProgressAction?.Invoke();
            await Navigation.PopAsync();
        }

        public string ItemName { get; private set; }
        public string EndItemText { get; private set; }
        public ICommand EndItemCommand { get; private set; }

        public INavigation Navigation { get; set; }

        private ItemProgress itemProgress;
        public ItemProgress ItemProgress
        {
            get { return itemProgress; }
            set { SetProperty(ref itemProgress, value); }
        }

        private List<LessonContent> contents;
        public List<LessonContent> ContentsProperty
        {
            get { return contents; }
            set { SetProperty(ref contents, value); }
        }        
    }
}
