﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Diagnostics;

namespace ZapoctakCSharp2018.ViewModel
{    
    /// <summary>
    /// Helping class for DataBinding. It allows you to bind LessonItem and ItemProgress together.
    /// </summary>
    public class ItemProgressViewModel : ProgressBaseViewModel
    {        
        public ItemProgressViewModel(LessonItem lessonItem, ItemProgress itemProgress, Action updateLessonAction = null)
        {
            item = lessonItem;
            progress = itemProgress;
            updateLessonAct = updateLessonAction;
            TypeIcon = Icons.GetIconByType(lessonItem.GetType());

            SetAct();
        }

        private Action updateLessonAct;
        public ImageSource TypeIcon { get; set; }

        // sets the progressState according to the item progress
        private void SetAct()
        {
            if (Progress.ActualProgress >= Progress.MaximumProgress)
            {
                ActStateOfItem = ProgressState.Done;
            }
            else if (Progress.ActualProgress >= 0)
            {
                ActStateOfItem = ProgressState.Working;
            }
            else
            {
                ActStateOfItem = ProgressState.Locked;
            }
        }

        public void SetActualProgress()
        {
            SetAct();
            updateLessonAct?.Invoke();            
        }

        public ProgressState ActStateOfItem
        {
            get { return actualProgressState; }
            set { SetProperty(ref actualProgressState, value); }
        }

        private LessonItem item;
        public LessonItem Item
        {
            get { return item; }
            set { SetProperty(ref item, value); }
        }

        private ItemProgress progress;
        public ItemProgress Progress
        {
            get { return progress; }
            set { SetProperty(ref progress, value); }
        }        
    }
}
