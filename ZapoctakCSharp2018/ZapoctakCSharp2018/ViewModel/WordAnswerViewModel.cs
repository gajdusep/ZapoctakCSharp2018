﻿using System;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// Helping ViewModel class. Used in Practices with check. Enables comfortable binding and manipulation with answers.
    /// </summary>
    public class WordAnswerViewModel : BaseViewModel
    {
        private Word word;
        public Word Word
        {
            get { return word; }
            set { SetProperty(ref word, value); }
        }

        // user's answer
        private string answer;
        public string Answer
        {
            get { return answer; }
            set { SetProperty(ref answer, value); }
        }

        // nullable bool, null means that user didn't answer yet
        private bool? resultCorrect;
        public bool? ResultCorrect
        {
            get { return resultCorrect; }
            set { SetProperty(ref resultCorrect, value); }
        }        
    }
}
