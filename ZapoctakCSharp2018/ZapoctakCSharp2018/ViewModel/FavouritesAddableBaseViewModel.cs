﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace ZapoctakCSharp2018.ViewModel
{
    /// <summary>
    /// Abstract viewModel class prepared for easy manipulation with Favourites. 
    /// Contains async virtual method StarClicked - bind with StarClickedCommand in views
    /// </summary>
    public abstract class FavouritesAddableBaseViewModel : BaseViewModel
    {
        // constants for paths to star icons
        protected static readonly string emptyStarPath = "emptystar.png";
        protected static readonly string fullStarPath = "fullstar.png";

        // Command for saving favourite words
        protected ICommand starClickedCommand;
        public ICommand StarClickedCommand
        {
            get { return starClickedCommand; }
            set { SetProperty(ref starClickedCommand, value); }
        }

        protected string starIconPath;
        public string StarIconPath
        {
            get { return starIconPath; }
            set { SetProperty(ref starIconPath, value); }
        }

        protected virtual async Task StarClicked()
        {
        }
    }
}
