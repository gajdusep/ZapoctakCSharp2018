﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ZapoctakCSharp2018
{
    public class ContentTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TableContentTemplate { get; set; }
        public DataTemplate ConversationContentTemplate { get; set; }
        public DataTemplate SimpleTextContentTemplate { get; set; }

        public ContentTemplateSelector()
        {
            //this.TableContentTemplate = new DataTemplate(typeof(GridUserControl));
            //this.ConversationContentTemplate = new DataTemplate(typeof(ConversationUserControl));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is TableContent)
            {
                return TableContentTemplate;
            }
            if (item is ConversationContent)
            {
                return ConversationContentTemplate;
            }
            if (item is TextContent)
            {
                return SimpleTextContentTemplate;
            }
            return TableContentTemplate;
        }
    }
}
