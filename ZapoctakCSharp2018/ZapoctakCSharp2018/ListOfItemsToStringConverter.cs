﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Xamarin.Forms;
using System.Globalization;

namespace ZapoctakCSharp2018
{
    public class ListOfStringsToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return "";
            }

            StringBuilder stringBuilder = new StringBuilder("");
            List<string> list = (List<string>)value;

            foreach (var item in list)
            {
                stringBuilder.Append(" * ");
                stringBuilder.Append(item);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // not needed...
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Class for converting list of lessons to single string 
    /// View is using this class then showing the list of all lesson items in single label
    /// </summary>
    public class ListOfItemsToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {                
                return "";
            }

            StringBuilder stringBuilder = new StringBuilder("");
            List<LessonItem> list = (List<LessonItem>)value;

            foreach (var item in list)
            {
                stringBuilder.Append(" * ");
                stringBuilder.Append(item.Name);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // not needed...
            throw new NotImplementedException();
        }
    }
}
