﻿# Czech-italian Textbook

## Programming Documentation

### Overview

This application is based on the italian textbook for czech students.

It is build on Xamarin (C#) and it was debugged only for Android platform.

This README contains all information about the code (I was asked to write it this way).

The user documentation is written in czech bellow.

---

### MVVM

I chose standart MVVM style of programming.
The Model classes "don't know anything" about ViewModel and View.
ViewModel is bound with View by the Xaml binding. 

### Model

The following classes hold the data of the whole book.
All model classes are XML serializable and the format is described directly by attributes.

#### Book 

Contains `List<Lesson>`, other properties can be added if needed, but currently it is not necessary.

#### Lesson 

Holds some information about Lesson in general (including name and path to the file).

Contains `List<LessonItem>`.

#### LessonItem

Abstract class. Every LessonItem holds a path to the file, where it can be read from.
Holds `List<LessonContent>` field - this property is used only in `GrammarItem` and `PracticeWithoutCheckItem`, but if any extension is needed, there can be used in another items.

Child classes:
- `GrammarItem`
- `VocabularyItem`
- `PracticeWithCheck`
- `PracticeNoCheck`

##### GrammarItem

In the current version, there is nothing but the fields of parent class.

##### VocabularyItem

Contains `List<Word>` - this list can be either list of real words with translations or the list of sentences with possible translations.

##### PracticeWithCheck

Contains `List<Word>` - list to translate. As in `VocabularyItem`, it can be a list of sentences to translate.

##### PracticeWithoutCheck

In the current version, there is nothing but the fields of parent class.

#### LessonContent

The smallest units of the model.
Every `LessonItem` can contain any of them. 
However in current implementation, only `GrammarItem` and `PracticeWithoutCheck` use them fully.

Child classes:
- `TextContent`
- `ConversationContent`
- `TableContent`
- `ImageContent`

##### TextContent 

Contains simple text in `string Text` property.

##### ConversationContent

Contains list of `Sentence` instances
and boolean property that says if the "speaker" of the sentence should be printed or not (sometimes the name or description is important (man, woman) and sometimes not, in that case, use name like "1", "2"...)

##### TableContent

The most complex LessonContent. 
- `string NameOfTable` property is the name that will be highlighted above the table.
- `List<string> TableHeader` may be null or contain the description of columns.
- `string Description` is the text describing the table, it gives more info about what is in the table.
- `List<List<string>> Table` contains the actual table data. The number of inputs in each column might be different, the right View of table is chosen in runtime.

#### Reading data

The programm uses two ways of reading the files.

- the data are stored in XML files that are added as **embedded resources**. 
This way is used for all classes directly bound with the book. 
It doesn't allow us to write to these files during the runtime.
- the data are stored by using PCLStorage library. This way is used for the user data and answers.
Generally for all files that need to be writen and read too (during the runtime).

##### XMLDataReader

For reading the data, there is the `XMLDataReader` class, 
that contains generic method `LoadXmlData<T>` that returns the object of type `T`.
This method is called asynchrounously every time any of model classes need to be deserialized.

This class is used reading embedded resources, that means all the files, that contain directly the book (`Book`, `Lesson`, `LessonItems`, `LessonContents`).

##### PCLStorageIO
Reading of `UserProgress` and `FavouriteWords` is done via `PCLStorage` class that works with local files
(this approach is necessary, because it is not possible to write into embedded files).


### View

In most cases the ViewModel is passed as a parameter to the constructor of a View and
`BindingContext` is set to the ViewModel.

I tried to write as few code as possible directly to the code behind and put everything to ViewModels.

Typical code behind in "SomeView.xaml.cs":

```
[XamlCompilation(XamlCompilationOptions.Compile)]
public partial class SomeView : ContentPage
{
    public SomeViewView (SomeViewModel viewModel)
    {
        InitializeComponent ();
        BindingContext = viewModel;
    }
}
```

In most cases the View is writen in XAML. 
Only exceptions are ConversationContent and GridUserControl. 
They are writen in C# code, because they need to be very dynamic
and the problems with `ListView` in `ListView` would occur, 
therefore, I decided to go this way.

Almost everywhere, where it is possible, I used data binding. 
Also, the binding of `Command`s is very helpful. 


### ViewModel

Every ViewModel in my solution inherits from `BaseViewModel`.
This class implements some methods, that make data binding easier. 

ViewModel Classes are in the `ZapoctakCSharp2018.ViewModel` namespace.

#### BaseViewModel

The most important ViewModel inherits from `INotifyPropertyChanged`. 
Every ViewModel inherits from this class. 
The `SetProperty` makes binding and ViewModels easier. 
In every ViewModel the bindable property can be writen as following:

```
private T someProperty;
public T SomeProperty
{
    get { return someProperty; }
    set { SetProperty(ref someProperty, value); }
}
```

Access SomeProperty from XAML as following:

```
<ContentView>
...
    <Label Text={Binding SomeProperty}/>
...
<ContentView>
```

#### ProgressBaseViewModel

View-model base class that has some information about progress on it.
From this class, `ItemProgressViewModel` and ` LessonViewModel` inherit.

#### FavouritesAddableBaseViewModel

Abstract ViewModel containing fields for comfortable work with Words or Sentences, that could be added to `Favourites`

#### Other ViewModels

Models for pages are usually passed as parameter in Views Constructors. 

The helping ViewModels (e.g. `WordWithinVocabularyItemViewModel`) are here for easier work with binding. 
They usually contain only a few fields, therefore they are sometimes used like Tuples, but they are more appropriate for most of the situations. 

### Helping and other classes

The program contains the following helping and other classes:

- TemplateSelectors - used for dynamic deciding which template to choose in `ListView`.
Mostly decides by type of objects.
  - `ContentTemplateSelector`
  - `PracticeContentTemplateSelector`
- `Icons` - class with only static members - `ImageSources`. 
Used for easier manipulation with icons. Unfortunately works only on Android, better work with files would have to be added for iOS.
- `Converters` - referenced from XAML, helps to easily transform some type into another
  - `ListOfStringsToStringConverter` - for showing the items in lesson without the need to open it

### Third party classes

#### CommandableListView

- BY DAVID HAUCK https://github.com/davidhauck/ItemClickBinding
- very useful class for `ListViews`. Enables us to use `ICommand` instead of `Action` and put everything into `ViewModels` and not code behind.

---

### Publishing and debugging

The whole app was tested only for Android and won't surely work on iOS (the biggest problem needed to be fixed is `Icons` class and generaly reading the files).

Because of the way the book classes are read, the whole book must be embedded before making the .apk file for Android phones.
(This way may be later changed, but currently it is like it is.)

#### Debugging

I found the usb connection the best way to debug. 
Plug your phone to your laptop and on play label should appear the name of your phone.
Just push it and the app should be launched on your phone.

#### Publishing

The whole procedure is described on https://docs.microsoft.com/cs-cz/xamarin/android/deploy-test/release-prep/?tabs=vswin
and here https://docs.microsoft.com/cs-cz/xamarin/android/deploy-test/signing/index?tabs=vswin.

---

## User documentation (in czech)

### Přehled

Program je aplikací pro výuku italštiny založený na učebnici italštiny od Aleny Bahníkové a dalších autorů.
Touto učebnicí se program víceméně řídí a po vložení dat v níže popsaném formátu může velkou část učebnice obsáhnout.


### Hlavní menu

Na úvodní (hlavní) obrazovce si může uživatel vybrat z těchto prvků: 

### Progress

Ve výchozím nastavení je progress zapnutý. 
Znamená to, že uživatel nemůže prohlížet lekce a prvky v lekcích (viz níže)
aniž by splnil lekce a prvky předchozí. 

### Výběr lekce

Každá lekce v sobě obsahuje soubor prvků. Prvky se dělí následovně:
prvek s gramatikou, 
prvek se slovíčky,
prvek s kontrolovaným cvičením a
prvek s cvičením bez kontroly.

### Výběr prvku

Na stránce s konkrétní lekcí je seznam prvků, které se v lekci nacházejí.
Prvky mohou být následující 
(u každého je uvedeno, jaké jsou podmínky pro navýšení progresu a tím pádem odemknutí dalších prkvů, případně lekcí):

#### Prvek s gramatikou

Prvek s gramatikou v sobě obsahuje jakkoliv smíchané tyto položky: 
- text
- konverzaci (libovolného počtu lidí),
- obrázky,
- pravidelné tabulky (každý sloupec má stejný počet řádků) a 
- nepravidelné tabulky (sloupce mají různý počet řádků).

Prvek je splněn po kliknutí na tlačítko, které je umístěno na stránce dole.

#### Prvek se slovíčky

Prvek obsahuje seznam slov (každé slovo může mít více překladů).
Kliknutím na hvězdičku si uživatel slovo může uložit do seznamu oblíbených slov.

Prvek je splněn po kliknutí na tlačítko, které je umístěno na stránce dole.

#### Prvek se cvičením s automatickou kontrolou

V tomto prvku uživatel píše odpověď do editoru. 
Odpověď se automaticky zkontroluje a pokud je správně, zadání úkolu už se neopakuje.
Pokud je špatně, zadání se zařadí do fronty slov a uživatel na něj musí odpovědět znovu.

Prvek je splněn po přeložení všech zadání.

#### Prvek se cvičením bez automatické kontroly

V tomto prvku uživatel sám pro sebe píše odpovědi na otázky. 
Tyto odpovědi se uloží po kliknutí na tlačítko pro uložení.
Uživatel musí vyplnit všechny políčka (sice jakýmkoliv textem, ale jako celá učebnice - i toto cvičení spoléhá na poctivost žáka).

